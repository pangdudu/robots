require 'rubygems'
require 'dbus'
require 'rofl'
#local requires
require 'robots_infrastructure'

class Robot
  attr_accessor :avahi #for mdns service discovery
  attr_accessor :emitters,:infrastructure,:listeners #stuff a robot needs
  attr_accessor :config #hash with lots of config magic inside
  
  def initialize
    dlog "new Robot initialized."
    @coreservice, @dbussession = nil,nil
    @listeners,@emitters,@infrastructure = [],{},{}
    @remote_robots_daemons = [] #we'll use avahi service discovery to fill this
    @system_bus = DBus::SystemBus.instance #we'll need this all over
    @active_busses = [] #we'll add them to the dbus loop later on
    @servicetypes = fill_servicetypes #refer to robots_infrastructure.rb
  end
  
  #start the mojo
  def run
    if @config.nil?
      elog "Agent not configured, exiting!"
      exit(0)
    end
    #we can always register the system (avahi) services, because they don't depend on the robots infrastructure
    register_system_callbacks
    #if our session is connecting to a special (remote robots) daemon, we need a little more care
    if @config[:sessiontype].eql? "robots"
      #if a host and a port were specified, we can connect directly
      unless @config[:daemonhost].eql?("avahi") and @config[:daemonport].eql?("avahi")
        setup_robots_service
        register_robots_callbacks
      else
        dlog "Using avahi auto daemon discovery!"
        dlog "Remote daemons found: #{@remote_robots_daemons.inspect}"
        exit(0)
      end
    end
    #if the session uses the system or session bus, everythings easy
    unless @config[:sessiontype].eql? "robots"
      setup_robots_service
      register_robots_callbacks      
    end
    #finally get into the dbus loop
  end

  #setup the robots service
  def setup_robots_service
    robots_running = running? @config[:robotsservice]
    dlog "robots service has already been started." if robots_running
    unless robots_running
      ilog "will now setup robots service infrastructure."
      start_core_service @config[:robotsservice]
      init_robots_infrastructure
    end
    get_robots_infrastructure
  end    

#ROBOTS SERVICE INIT START

=begin
  the following methods are only called, if the robots service is not already running 
=end 
 
  #setup the infrastructure objects for communication
  def init_robots_infrastructure
    emitter = Emitter.new(config[:emitterpath],self)
    @coreservice.export(emitter)
    @infrastructure["emitter"] = emitter
  end

  #get instances of our infrastructure objects
  def get_robots_infrastructure
    emitter = get_object_from_service @config[:robotsservice],@config[:emitterpath],@config[:emitterinterface]
    @emitters["robots"] = emitter
  end
  
  #start a service
  def start_core_service service_name=@config[:robotsservice]
    bus = get_bus
    #first of all check if the service we want to launch is already started
    dlog "skipping service: #{service_name} has already been started." if running? service_name 
    unless running? service_name
      begin
        @coreservice = bus.request_service(service_name)
      rescue
        elog "service request for #{service_name} denied."
      end
    end
  end

#ROBOTS SERVICE INIT END
  
  #get a dbus object from a service
  def get_object_from_service service_name,obj_path,default_iface
    begin
      if running? service_name
        bus = get_bus
        ruby_service = bus.service(service_name)
        obj = ruby_service.object(obj_path)
        obj.introspect
        obj.default_iface = default_iface
        return obj
      end
      return nil
    rescue
      elog "could not get object:#{obj_path} from service: #{service_name}"
      return nil
    end
  end 
   
  #register callbacks for avahi service discovery and other useful services from the local system bus
  def register_system_callbacks
    #old systems sometimes don't have avahi
    if @system_bus.proxy.ListNames[0].include? "org.freedesktop.Avahi"
      #setup service browser
      #bus = DBus::SystemBus.instance #needs to be the system bus for the avahi stuff
      proxy = @system_bus.introspect("org.freedesktop.Avahi","/")
      @avahi = proxy["org.freedesktop.Avahi.Server"]  
      @servicetypes.each { |name,type| register_service_callback name,type }
    else
      wlog "Sorry, no Avahi service running, no auto service discovery."
      #check if we depend on avahi running
      if @config[:daemonhost].eql?("avahi") and @config[:daemonport].eql?("avahi") and @config[:sessiontype].eql?("robots")
        elog "Sorry mate, can't continue without avahi. Change your 'sessiontype' to 'system' or 'session', will now exit."
        exit(0) #cruel, I know, but it makes no sense to continue, 
      end
    end
  end

  #builds a service type specific callback
  def register_service_callback name,type
    #bus = DBus::SystemBus.instance #needs to be the system bus for the avahi stuff
    sb = @avahi.ServiceBrowserNew(-1,-1,"_#{type}._tcp","local",0)
    #now we start the match rule definition
    mr = DBus::MatchRule.new
    mr.type = "signal"
    mr.interface = "org.freedesktop.Avahi.ServiceBrowser"
    mr.path = sb.first
    @system_bus.add_match(mr) { |msg| service_callback name,msg } unless type.eql? "robots"
    #our service is handled specialy
    @system_bus.add_match(mr) { |msg| robots_daemon_callback name,msg } if type.eql? "robots" 
  end
 
  #gets called on a service change
  def service_callback name,msg
    if msg.member.eql? "ItemNew"
      dlog "new #{name}: #{msg.params[2]}"
    end
    if msg.member.eql? "ItemRemoved"
      dlog "removed #{name}: #{msg.params[2]}"
    end
  end

  #gets called when a robots daemon has been found
  def robots_daemon_callback name,msg
    if msg.member.eql? "ItemNew"
      dlog "new robots daemon #{name}: #{msg.params[2]}"
    end
    if msg.member.eql? "ItemRemoved"
      dlog "robots daemon removed #{name}: #{msg.params[2]}"
    end
  end
  
  #register robots callback methods
  def register_robots_callbacks
    #setup callbacks for infrastructure emitters
    @emitters.each { |name,emitter| register_emitter_callback name,emitter }
  end

  #register an emitter callback
  def register_emitter_callback name,emitter
    bus = get_bus
    #dlog "emitter.inspect #{emitter.inspect}"
    dlog "registering callback for #{name} on path #{emitter.path}"
    mr = DBus::MatchRule.new
    mr.type = "signal"
    mr.interface = @config[:emitterinterface]
    mr.path = emitter.path
    bus.add_match(mr) { |msg| emitter_proxy_callback msg }
  end
  
  #get called when receiving an remote emitter message
  def emitter_proxy_callback msg
    @listeners.each { |l| l.receive_msg msg.params[0] } unless msg.params[0].nil?
  end
  
  #gets called from the exported emitter object
  def emitter_callback msg
     #when other agents use the send_message of the proxy, it will only reach the
     #local non proxy object, so we need to emit a signal and tell everyone 
     @infrastructure["emitter"].newMessage(msg) if  @infrastructure.has_key? "emitter"
  end
    
  #check if a service is running
  def running? service_name
    bus = get_bus
    return bus.proxy.ListNames[0].include? service_name
  end

  #send a message over the robots service
  def send_msg msg
    @emitters["robots"].send_message(msg) unless @emitters["robots"].nil?
    wlog "emitter is nil, can not send message." if @emitters["robots"].nil?
  end
  
  #add a listener
  def add_listener listener
    #check if the method is implemented
    impcheck = defined? listener.receive_msg 
    #if it is add to listeners
    @listeners << listener if impcheck.eql? "method"
    #if not inform dev
    unless impcheck.eql? "method" 
      elog "Method 'receive_msg string' needs to be implemented! Example:"
      puts "def receive_msg msg\n  puts msg\nend"  
    end
  end

  #get the bus session, cleaner then creating new sessions all over
  def get_bus
    if @dbussession.nil?
      @dbussession = @system_bus if @config[:sessiontype].eql? "system"
      @dbussession = DBus::SessionBus.instance if @config[:sessiontype].eql? "session"
      if @config[:sessiontype].eql? "robots"
       DBus.const_set("SessionSocketName", @config[:robots_socket_name]) #overwrite the modules constant
       get_remote_cookie if @config[:sshcookie] #i need not say more,...
       @dbussession = DBus::SessionBus.instance
      end
      #need to add them to the dbus_loop later on
      @active_busses << @dbussession unless @active_busses.include? @dbussession
    end
    #no point if this is still true :)
    if @dbussession.nil?
      elog "@dbussession is nil! will exit now, sorry."
      exit(0)
    end
    return @dbussession
  end

  #because dbus remote auth still betrays us, we need to hack it
  def get_remote_cookie
    if @config[:robots_socket_name].include? "tcp:"
      af, port, daemon_name, daemon_addr = (Socket::getaddrinfo(@config[:daemonhost],@config[:daemonhost].to_i)).first
      ilog "Trying to get/hack remote cookie from: #{daemon_name}."
      begin
        cookiepath = "#{ENV['HOME']}/.dbus-keyrings/org_freedesktop_general"
        #oki, the scp magic only works, if you use sshkeys
        `scp #{daemon_addr}:.dbus-keyrings/org_freedesktop_general #{cookiepath}`
        if File.exist? cookiepath
          cookie = File.open(cookiepath) 
          dlog "Got cookie: #{cookie.gets}"
        end
      rescue
        elog "Oops, something is wrong."
      end
    else
      dlog "Nothing to hack, boring."
    end
  end
 
  #list names of a proxy object
  def list_names proxy=nil
    if proxy.nil?
      bus = get_bus
      proxy = bus.proxy
    end
    dlog "listnames:"
    proxy.ListNames[0].each { |name| dlog name }
  end
  
  #loop we need to stay tuned,broken
  def dbus_loop
    Thread.new do
      main = DBus::Main.new
      @active_busses.each { |bus| main << bus }
      main.run
    end
  end
end
