require 'rubygems'
require 'rofl' #gem install pangdudu-rofl --source=http://gems.github.com
require 'robots'
require 'robots_xml'

class RobotsTest
  include Robots
  include RobotsXml
  
  attr_accessor :roboname
  
  def initialize
    @roboname = "Robots Prototype" #name used in alive messages
    ilog "#{@roboname} initialized"
  end
  
  #method that gets called when a new message arrives
  def receive_msg msg
    check_for_interests msg
  end
  
  #check if this msg interests us
  def check_for_interests xml_msg
    #message filter callback looks like this now
    info = parse_xml_msg xml_msg,"//info"
    dlog info unless info.empty?
    #and one that doesn't work, unless you add this tag to the xml
    notfound = parse_xml_msg xml_msg,"//thereisnosuchtag"
    dlog notfound unless notfound.empty?
  end
  
end

rt = RobotsTest.new
rt.release_robots #start the robot agent module
rt.robotsagent.list_names #for debug

loop do 
  sleep 5
  #send an xml message over the system
  rt.send_msg rt.create_xml_alive_msg 
end
