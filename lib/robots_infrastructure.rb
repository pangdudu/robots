=begin
this includes all the necessary dbus objects we need for the robots infrastructure
=end
require 'rubygems'
require 'dbus' #gem install pangdudu-ruby-dbus --source=http://gems.github.com
require 'rofl' #gem install pangdudu-rofl --source=http://gems.github.com 

#basic emitter class
class Emitter < DBus::Object
  
  attr_accessor :listener
  
  def initialize path,listener
    super path
    @listener = listener
    dlog "new Emitter initialized at #{path}"
  end
  
  #create an dbus interface
  dbus_interface "org.robots.Service.EmitterInterface" do
    #creates a signal in the interface:
    dbus_signal :newMessage, "msgbody:s"
    #creates an emit method in the interface:
    dbus_method :send_message, "in msg:s" do |msg|
      send_message_callback msg
    end
  end #end of the dbus interface
  
  #this method gets called by remote proxy objects calling send_message
  def send_message_callback msg
    @listener.emitter_callback msg
  end
end

#hardcoding i don't want in the agent file
def fill_servicetypes
  types = {}
  types["Robots IPC cluster server"] = "robots"
  types["Workstation"] = "workstation"
  types["SSH Remote Terminal"] = "ssh"
  #if you like debug output, uncomment the following lines  
  #types["Website"] = "http"
  #types["Secure Website"] = "https"
  #types["iChat Presence"] = "presence"
  #types["PulseAudio Sound Server"] = "pulse-server"
  #types["Subversion Revision Control"] = "svn"
  #types["GIT"] = "git"
  #types["APT Package Repository"] = "apt"
  #types["WebDAV"] = "webdav"
  #types["Secure WebDAV"] = "webdavs"
  #types["Samba"] = "smb"
  return types
end
