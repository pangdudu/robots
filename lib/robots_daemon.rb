#check if a dbus-daemon suitable for robots is running, otherwise start one
def check_for_dbus_daemon
  unless @remote
    @daemonconfig = File.expand_path(@daemonconfig) #dbus-daemon won't accept otherwise
    #ok, no config file, not good!
    unless File.exist? @daemonconfig
      wlog "no daemon config: #{@daemonconfig}!"
      wlog "will sleep for 5 seconds,..."
      sleep 5 #give the user time to realize
    end
    #if the config file is there
    if File.exist? @daemonconfig  
      @dbusdaemon = DbusDaemon.new @daemonconfig
      got_twins = @dbusdaemon.got_twin_daemon? #check if a daemon like this is already running
      ilog "custom dbus-daemon is already running." if got_twins #inform user
      unless got_twins #no suitable dbus-daemon running, start one
        @dbusdaemon.start_daemon
        ilog "custom dbus-daemon started and running, check \"ps x | grep \"dbus-daemon\", if plan a CTRL-C."
        #@dbusdaemon.stop_daemon #must get called sometimes, super nasty, use " ps x | grep "dbus-daemon" "
      end
    end
  end
end
