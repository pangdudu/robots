require 'socket' #to get hostname etc.
require 'rubygems'
require 'rofl' #gem install pangdudu-rofl --source=http://gems.github.com
#local requires
require 'robots_agent'

module Robots
  
  attr_accessor :robotsagent,:hostname,:config
  
  #release the robots
  def release_robots
    check_robotsagent
    @hostname = @config[:localhost]
    socket_name = "tcp:host=#{@config[:daemonhost]},port=#{@config[:daemonport]},family=ipv4"
    @config[:robots_socket_name] = socket_name #look at: config/remote.session.dbus.conf
    #the final steps, to make the robot ready
    @robotsagent.config = @config
    @robotsagent.run
    @robotsagent.add_listener self
    @robots_running = true
  end
  
  #OVERWRITE FOLLOWING METHODS:
  
  #receives messages from the robots, should be overwritten in the implementing class
  def receive_msg msg
    ilog msg
  end

  #sends messages to the robots, should be overwritten in the implementing class
  def send_msg msg
    if @robots_running
      @robotsagent.send_msg msg
    else
      wlog "Robot not yet running, can't send message."
    end
  end
  
  #OVERWRITE END
  
  #check if robots are set up
  def check_robotsagent
    @haz_robots = @robotsagent.nil?
    if @haz_robots
      @robotsagent = Robot.new
    end
    if @config.nil?
      wlog "No config supplied! Will use default config."
      @config = get_default_config
    end 
    return @haz_robots
  end
  
  #this is the default config, you can modify the values, than pass it to the agent  
  def get_default_config
    config = {}
    config[:localhost] = Socket.gethostname
    config[:daemonhost] = "localhost"
    config[:daemonport] = "2687"
    config[:sshcookie] = false #if you want to connect remotely, change this value
    config[:sessiontype] = "robots" #could be session,system or robots (use robots, if you want it to work over multiple hosts)
    config[:robotsservice] = "org.robots.Service"
    config[:emitterpath] = "/org/robots/Service/Emitter"
    config[:emitterinterface] = "org.robots.Service.EmitterInterface"
    return config
  end
end
