require 'rubygems'
require 'rofl' #gem install pangdudu-rofl --source=http://gems.github.com
require 'hpricot' #my favourite xml parser
require 'builder' #very simple xml builder

module RobotsXml
   
  #create an xml message
  def create_xml_msg &block
    b = Builder::XmlMarkup.new
    return b.msg &block
  end
  
  #parse a xml message
  def parse_xml_msg xml_msg,xpath="//msg"
    begin
      xml = Hpricot.XML(xml_msg)
      msg = (xml/xpath)
      return msg
    rescue
      wlog "message parsing error!"
      return xml = Hpricot.XML("<msg>parsing error</msg>")
    end
  end
  
  #SPECIALS
  
  #create an alive message
  def create_xml_alive_msg
    @localhost = "localhost" if @localhost.nil? #more fun with hostname
    @roboname = "Nameless" if @roboname.nil? #at least it's nameless
    robotsid = "#{@localhost}.#{@roboname.gsub(" ","_")}" #looks cooler
    msg = create_xml_msg { |b| b.info("#{@roboname} alive","alive"=>"#{robotsid}","timestamp" => "#{Time.now}") }
    return msg
  end
    
end

