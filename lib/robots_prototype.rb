require 'rubygems'
require 'rofl' #gem install pangdudu-rofl --source=http://gems.github.com
require 'robots'
require 'robots_xml'

#the RobotsXml module supplies nice methods for xml handling
#create xml messages like this: xml_msg = create_xml_msg { |b| b.body("test"); b.info("timestamp" => "#{Time.now}") }
#send them like this: send_msg xml_msg
#parse them like this: parse_xml_msg xml_msg,xpath

class RobotsProto
  include Robots
  include RobotsXml
  
  attr_accessor :roboname,:config
  
  def initialize
    #rofl_enable_trace
    @roboname = "Robots Prototype" #name used in alive messages
    configure #configure the robot
    ilog "#{@roboname} initialized"
  end
  
  #configure the robot using a default config
  def configure
    @config = get_default_config #get the default config from the module
    #config values
    @config[:sessiontype] = "robots" #could be session,system or robots (use robots, if you want it to work over multiple hosts)
    @config[:daemonhost] = "hanzo"
    @config[:daemonport] = "2687"
    @config[:sshcookie] = true #if you want to connect remotely, you need to get the daemon cookie over ssh
  end
  
  #method that gets called when a new message arrives
  def receive_msg msg
    check_for_interests msg
  end
  
  #check if this msg interests us
  def check_for_interests xml_msg
    #message filter callback looks like this now
    info = parse_xml_msg xml_msg,"//info"
    dlog info unless info.empty?
  end
  
end

#start the app
rp = RobotsProto.new
rp.release_robots #start the robot agent module

#build you own loop if you need one
loop do 
  sleep 1
  #send an xml message over the system
  puts "send msg"
  alive_msg = rp.create_xml_alive_msg
  rp.send_msg alive_msg
end
