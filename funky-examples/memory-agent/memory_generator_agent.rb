require 'rubygems'
require 'rofl' #sudo gem install pangdudu-rofl --source=http://gems.github.com
require 'robots' #sudo gem install pangdudu-robots --source=http://gems.github.com
require 'robots_xml'

#the RobotsXml module supplies nice methods for xml handling
#create xml messages like this: xml_msg = create_xml_msg { |b| b.body("test"); b.info("timestamp" => "#{Time.now}") }
#send them like this: send_msg xml_msg
#parse them like this: parse_xml_msg xml_msg,xpath

class MemoryGeneratorAgent
  include Robots
  include RobotsXml
  
  attr_accessor :roboname
  
  def initialize
    @roboname = "Memory Generator" #name used in alive messages
    configure #configure the robot
    ilog "#{@roboname} initialized"
  end
  
  #configure the robot using a default config
  def configure
    @config = get_default_config #get the default config from the module
    #config values
    @config[:sessiontype] = "robots" #could be session,system or robots (use robots, if you want it to work over multiple hosts)
    @config[:daemonhost] = "localhost"
    @config[:daemonport] = "2687"
    @config[:sshcookie] = false #if you want to connect remotely, you need to get the daemon cookie over ssh
  end
  
  #ROBOTS message parsing stuff
  
  #method that gets called when a new message arrives
  def receive_msg msg
    check_for_interests msg
  end
  
  #check if this msg interests us
  def check_for_interests xml_msg
    #message filter callback looks like this now
    memory = parse_xml_msg xml_msg,"//memory"
    dlog memory unless memory.empty?
    #message filter callback looks like this now
    info = parse_xml_msg xml_msg,"//info"
    dlog info unless info.empty?
  end
  
end

#start the app
mga = MemoryGeneratorAgent.new
#begin

mga.release_robots #start the robot agent module

#build you own loop if you need one
loop do 
  sleep 3
  #send an xml message over the system
  mga.send_msg mga.create_xml_alive_msg
  #generate some bogus memory remember
  mga.send_msg mga.create_xml_msg { |b| b.memory("something happened","action" => "remember");b.question("what happened?","action"=>"remember");b.keyword("stuff");b.keyword("happening") }
  #generate bogus ask question
  mga.send_msg mga.create_xml_msg { |b| b.memory("action" => "question");b.question("what happened?","action"=>"ask") }
  #generate bogus retrieve keywords
  mga.send_msg mga.create_xml_msg { |b| b.memory("something happened","action" => "keywords");}
  #generate bogus retrieve memories from keywords
  mga.send_msg mga.create_xml_msg { |b| b.memory("action" => "memories");b.keyword("stuff");b.keyword("happening") }
end

#end
