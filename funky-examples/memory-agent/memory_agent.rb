require 'rubygems'
require 'dm-core' #sudo gem install datamapper
require 'do_mysql' #sudo gem install do_mysql
require 'dm-timestamps' #sudo gem install dm-timestamps
require 'rofl' #sudo gem install pangdudu-rofl --source=http://gems.github.com
require 'robots' #sudo gem install pangdudu-robots --source=http://gems.github.com
require 'robots_xml'

#the RobotsXml module supplies nice methods for xml handling
#create xml messages like this: xml_msg = create_xml_msg { |b| b.body("test"); b.info("timestamp" => "#{Time.now}") }
#send them like this: send_msg xml_msg
#parse them like this: parse_xml_msg xml_msg,xpath

#do the mysql datamapper setup
DataMapper.setup(:default,{:adapter => 'mysql',:host => 'localhost',:username => 'robots',:password => 'robots!secret',:database => 'robots'})

#if you don't want to use a mysql database, or just need raw speed, check this out
#DataMapper.setup(:in_memory, :adapter => 'in_memory')

#and define a memory class
class Memory
  include DataMapper::Resource
  property :index,      Serial
  property :value,      Text
  property :created_at, DateTime
  property :created_on, Date
  property :updated_at, DateTime
  property :updated_on, Date
  #property :deleted_at, ParanoidDateTime
  has n,  :questions, :through => Resource
  has n,  :keywords,  :through => Resource
end

#and define a memory class
class Question
  include DataMapper::Resource
  property :index,      Serial
  property :value,      Text
  property :created_at, DateTime
  property :created_on, Date
  property :updated_at, DateTime
  property :updated_on, Date
  #property :deleted_at, ParanoidDateTime
  has n,  :memories, :through => Resource
end

#memories may have keywords
class Keyword
    include DataMapper::Resource
    property :index,      Serial
    property :value,      String
    property :created_at, DateTime
    property :created_on, Date
    property :updated_at, DateTime
    property :updated_on, Date
    #property :deleted_at, ParanoidDateTime
    has n,  :memories, :through => Resource
end

#if your records aren't persistent, try removing the migrate statements
DataMapper.auto_migrate!
#Memory.auto_migrate!
#Question.auto_migrate!
#Keyword.auto_migrate!

class MemoryAgent
  include Robots
  include RobotsXml
  
  attr_accessor :roboname
  
  def initialize
    @roboname = "Active Memory" #name used in alive messages
    configure #configure the robot
    ilog "#{@roboname} initialized"
  end
  
  #configure the robot using a default config
  def configure
    @config = get_default_config #get the default config from the module
    #config values
    @config[:sessiontype] = "robots" #could be session,system or robots (use robots, if you want it to work over multiple hosts)
    @config[:daemonhost] = "localhost"
    @config[:daemonport] = "2687"
    @config[:sshcookie] = false #if you want to connect remotely, you need to get the daemon cookie over ssh
  end
  
  #process an incoming memory message
  def process_memory_msg xml_msg
    memories = get_memories xml_msg
    questions = get_questions xml_msg
    keywords = get_keywords xml_msg
    #remember a memory 
    #rather check for Array class here, lazy...
    unless (memories.eql? "question") || (memories.eql? "keywords") || (memories.eql? "memories")
      memories.each {|m| process_memory m,questions,keywords} 
    end
    #ask a question and return memories
    if memories.eql? "question"
      questions.each {|q| process_question q}
    end
    #retrieve memories for keywords
    if memories.eql? "memories"
      process_keywords keywords
    end
    #retrieve keywords for a memory
    #if memories.eql? "keywords"  
    #end
  end
  
  #process a single memory
  def process_memory memory,questions,keywords
    questions.each {|q| memory.questions << q}
    keywords.each {|k| memory.keywords << k}
    #finally save the updated memory to the storage
    memory.save
  end
  
  #process a question
  def process_question question
    qval = question.value
    #get all question with that value from storage
    questions = Question.all(:value => qval)
    memories = {} #single values behind key, so only newest memory will be returned
    #get all associated memories from storage
    questions.each { |q| q.memories.each { |m| memories[m.value] = m } }
    memories.each do |n,m| 
      #create a response message
      msg = create_xml_msg { |b| b.memory_response(m.value,"action"=>"question-response","question"=>qval,"timestamp" => "#{Time.now}") }
      #send response message to system
      send_msg msg
    end
  end
  
  #process keywords, and return matching memories to system
  def process_keywords keywords
    memories = {}
    keys = []
    #the following could be a super nasty oneliner :)
    keywords.each do |keyword|
      keys << keyword.value #use that later for sorting
      (Keyword.all(:value => keyword.value)).each do |k|
        k.memories.each do |m|
          if memories.has_key? m.value
            memories[m.value][k.value] = "key" #add another keyword
          else
            memories[m.value] = {k.value => "key"} #it's a new entry
          end
        end
      end
    end
    #oki, we now know which memories have what keywords, now we sort out the good ones
    cinderella = {}
    memories.each do |value,keywords|
      good = true
      keys.each { |key| good = false unless keywords.has_key? key }
      #send message if we're good
      if good
        keys_string = ""
        keys.each { |k| keys_string += "#{k} " }
        msg = create_xml_msg { |b| b.memory_response(value,"action"=>"keywords-response","keywords"=>keys_string,"timestamp" => "#{Time.now}") }
      end
    end
    #ok, there probably would have been a more datamapperish way to do this, tell me if you know one :)
    #dlog cinderella.inspect
  end
  
  #get the memories from an xml_msg
  def get_memories xml_msg
    memories = []
    mems = parse_xml_msg xml_msg,"//memory"
    mems.each do |m| 
      if m.attributes["action"].eql? "remember"
        memory = Memory.new(:value => m.inner_text)
        memory.save
        memories << memory
      end
      return "question" if m.attributes["action"].eql? "question" #ask a question, retrieve memory
      return "keywords" if m.attributes["action"].eql? "keywords" #retrieve keywords for memory - not implemented yet
      return "memories" if m.attributes["action"].eql? "memories" #retrieve memories for keywords
    end
    return memories
  end
  
  #get the keywords from an xml_msg
  def get_keywords xml_msg
    keywords = []
    keys = parse_xml_msg xml_msg,"//keyword"
    keys.each do |k| 
      keyword = Keyword.new(:value => k.inner_text)
      keyword.save
      keywords << keyword
    end
    return keywords
  end

  #get the questions from an xml_msg
  def get_questions xml_msg
    questions = []
    quests = parse_xml_msg xml_msg,"//question"
    quests.each do |q| 
      question =  Question.new(:value => q.inner_text)
      questions << question
      question.save if q.attributes["action"].eql? "remember"
    end
    return questions
  end
  
  #ROBOTS message parsing stuff
  
  #method that gets called when a new message arrives
  def receive_msg msg
    check_for_interests msg
  end
  
  #check if this msg interests us
  def check_for_interests xml_msg
    #message filter callback looks like this now
    memory = parse_xml_msg xml_msg,"//memory"
    process_memory_msg xml_msg unless memory.empty?
  end
=begin
  memory messages:
  
  remember a memory:
  <msg>
    <memory action="remember">something happened</memory>
    <question action="remember">what happened?</question>
    <keyword>stuff</keyword>
    <keyword>happening</keyword>
  </msg>
  
  ask a question:
  <msg>
    <memory action="question"></memory>
    <question action="ask">what happened?</question>
  </msg>
  
  retrieve memories matching keywords:
  <msg>
    <memory action="memories"></memory>
    <keyword>stuff</keyword>
    <keyword>happening</keyword>
  </msg>
  
  not yet implemented, to lazy, retrieve keywords:
  <msg>
    <memory action="keywords">something happened</memory>
  </msg>
=end
end

#start the app
ma = MemoryAgent.new
#=begin
ma.release_robots #start the robot agent module

#build you own loop if you need one
loop do 
  sleep 1
  #send an xml message over the system
  ma.send_msg ma.create_xml_alive_msg
end
#=end
