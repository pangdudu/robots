require 'rubygems'
require 'swiftly' #have a look at: http://github.com/pangdudu/swiftly/tree/master
require 'rofl' #sudo gem install pangdudu-rofl --source=http://gems.github.com
require 'robots' #sudo gem install pangdudu-robots --source=http://gems.github.com
require 'robots_xml'

#the RobotsXml module supplies nice methods for xml handling
#create xml messages like this: xml_msg = create_xml_msg { |b| b.body("test"); b.info("timestamp" => "#{Time.now}") }
#send them like this: send_msg xml_msg
#parse them like this: parse_xml_msg xml_msg,xpath

class SwiftAgent
  include Swiftly
  include Robots
  include RobotsXml
  
  attr_accessor :roboname
  
  def initialize
    @roboname = "Swift Talker" #name used in alive messages
    configure #configure the robot
    ilog "#{@roboname} initialized"
  end
  
  #configure the robot using a default config
  def configure
    @config = get_default_config #get the default config from the module
    #config values
    @config[:sessiontype] = "robots" #could be session,system or robots (use robots, if you want it to work over multiple hosts)
    @config[:daemonhost] = "localhost"
    @config[:daemonport] = "2687"
    @config[:sshcookie] = false #if you want to connect remotely, you need to get the daemon cookie over ssh
  end
  
  #method that gets called when a new message arrives
  def receive_msg msg
    check_for_interests msg
  end
  
  #check if this msg interests us
  def check_for_interests xml_msg
    #message filter callback looks like this now
    text = parse_xml_msg xml_msg,"//speak"
    speak text.inner_text unless text.empty?
  end
  
end

#start the app
sa = SwiftAgent.new
sa.release_robots #start the robot agent module

#build you own loop if you need one
loop do 
  sleep 1
  #send an xml message over the system
  sa.send_msg sa.create_xml_alive_msg
end
