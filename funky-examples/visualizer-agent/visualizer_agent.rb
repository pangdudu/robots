require 'rubygems'
require 'Qt4'
require 'flow_physics'
require 'qt_visual'
#require 'rofl' #sudo gem install pangdudu-rofl --source=http://gems.github.com
require 'robots' #sudo gem install pangdudu-robots --source=http://gems.github.com
require 'robots_xml'

class VisualizerAgent
  include Robots
  include RobotsXml

  attr_accessor :roboname
  
  def initialize argv
    @roboname = "Visualizer" #name used in alive messages
    @width, @height = 800,600
    @app = Qt::Application.new(argv)
    @agents = {} #thats where we register the agents in
    configure #configure the robot
    ilog "#{@roboname} initialized"
  end
  
  #configure the robot using a default config
  def configure
    @config = get_default_config #get the default config from the module
    #config values
    @config[:sessiontype] = "robots" #could be session,system or robots (use robots, if you want it to work over multiple hosts)
    @config[:daemonhost] = "localhost"
    @config[:daemonport] = "2687"
    @config[:sshcookie] = false #if you want to connect remotely, you need to get the daemon cookie over ssh
  end
  
  #process an info message
  def process_info xml_msg
    infos = parse_xml_msg xml_msg,"//info"
    infos.each do |i| 
      unless i.attributes["alive"].nil?
        agentname = i.attributes["alive"]
        @agents[agentname] = "#{i.attributes["timestamp"]}"
        unless @gui.nil?
          x_rnd = 23-rand(46)/2
          y_rnd = 23-rand(46)/2
          @gui.add_agent Agent.new(agentname,@width/2+x_rnd,@height/2+y_rnd) unless @gui.agents.has_key? agentname
        end
        #cleanup gui agent model - not yet implemented
      end
    end
  end
  
  #ROBOTS message parsing stuff
  
  #method that gets called when a new message arrives
  def receive_msg msg
    check_for_interests msg
  end
  
  #check if this msg interests us
  def check_for_interests xml_msg
    #message filter callback looks like this now
    info = parse_xml_msg xml_msg,"//info"
    process_info xml_msg unless info.empty?
  end
  
  def start_qt_visualizer
    @gui = Gui.new
    @physics = Physics.new(@width,@height,@gui)
    #dirty qt timer magic to make ruby threads work
    sleep 0.5
    block=Proc.new{ Thread.pass }
    timer=Qt::Timer.new(@gui)
    invoke=Qt::BlockInvocation.new(timer, block, "invoke()")
    Qt::Object.connect(timer, SIGNAL("timeout()"), invoke, SLOT("invoke()"))
    sleep 0.5
    timer.start(1000/60) #in millis
    #end of dirty timer hack
    @gui.physics = @physics
    @gui.resize(@width, @height)
    @gui.show()
    #just because it's fun
    #add_dbug_agents
    @app.exec()
  end
  
  #debug method to add agents quickly
  def add_dbug_agents
    dbug_agents = ["Jeanette","Kurt","Maurice","Patrice"]
    dbug_agents.each do |agentname|
      x_rnd = 23-rand(46)/2
      y_rnd = 23-rand(46)/2
      @gui.add_agent Agent.new("dbug.#{agentname}",@width/2+x_rnd,@height/2+y_rnd)
    end
  end
end

#start the app
va = VisualizerAgent.new(ARGV)
va.release_robots #start the robot agent module

Thread.new do
  sleep 1
  #build you own loop if you need one
  loop do 
    sleep 1
    #send an xml message over the system
    alive_msg = va.create_xml_alive_msg
    dlog "alive_msg: #{alive_msg.to_s}"
    va.send_msg alive_msg
  end
end

va.start_qt_visualizer

