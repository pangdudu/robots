require 'Qt4'
require 'flow_physics'

class Agent
  attr_accessor :x, :y, :rel_angle, :agentname

  def initialize agentname, x=0, y=0
    @agentname = agentname
    @x, @y=  x, y
  end

  #get the relative angle
  def get_rel_angle x,y
    #angle in radian (up -0.5, left -1/1, down 0.5, right 0)
    @rel_angle = Math.atan2(@y-y,@x-x)/Math::PI
    #we want a range from -1 to +1
    @rel_angle = @rel_angle * 180
    return @rel_angle
  end
end

class DragInfo
  attr_accessor :x, :y, :object

  def initialize startx, starty, object=nil
    @x, @y, @object = startx, starty, object
  end
end

class Gui < Qt::Widget
  
  attr_accessor :agents,:physics, :updating

  @@r = 15 #circle radius
  
  def initialize(parent = nil)
    super()
    @agents = {}
    setPalette(Qt::Palette.new(Qt::Color.new(250, 250, 250)))
    setAutoFillBackground(true)
    @cur1 = self.cursor
    @cur2 = Qt::Cursor.new(Qt::PointingHandCursor)
    self.mouseTracking = true
    @updating = false
  end
  
  #add an agent to the model
  def add_agent agent
    @agents[agent.agentname] = agent
    @physics.add_object agent unless @physics.nil?
    puts "Agent: #{agent.agentname} added."
  end

  #gets called when a repaint is necessary
  def paintEvent(event)
    @updating = true
    painter = Qt::Painter.new(self)
    painter.setRenderHint(Qt::Painter::Antialiasing)
    painter.setBrush(Qt::Brush.new(Qt::Color.new(255,250,250,150)))
    @agents.each do |agentname,agent|
      painter.resetMatrix
      painter.translate(agent.x,agent.y)
      painter.setPen(Qt::Pen.new(Qt::blue))
      painter.drawEllipse Qt::Rect.new(-@@r,-@@r, 2*@@r, 2*@@r)
      painter.setPen(Qt::Pen.new(Qt::blue))
      painter.drawEllipse Qt::Rect.new(-3*@@r,-3*@@r, @@r/2, @@r/2)
      painter.setPen(Qt::Pen.new(Qt::blue))
      painter.drawLine Qt::LineF.new(-2.5*@@r,-2.5*@@r, -0.8*@@r, -0.8*@@r)
      painter.setPen(Qt::Pen.new(Qt::blue))
      painter.drawText(-2*@@r,-2.5*@@r,"#{agentname}")
    end
    painter.end()
    @updating = false
  end
  
  #mouse press event
  def mousePressEvent event
    @draginfo = nil
    @physics.singularity = true unless @physics.nil?
    if event.buttons == Qt::RightButton
    elsif event.buttons == Qt::LeftButton
      agent = object_at(event.x, event.y)
      @draginfo = DragInfo.new(event.x, event.y, agent) unless agent.nil?
    end
  end
  
  #returns object at x,y
  def object_at x,y
    @agents.each do |name,agent|
      return agent if (agent.x-x)**2 + (agent.y-y)**2 <= @@r**2
    end
    return nil
  end

  #release mouse event
  def mouseReleaseEvent event
    update_drag_object event.x, event.y unless @draginfo.nil?
    @physics.singularity = false unless @physics.nil?
  end

  #update dragged object
  def update_drag_object x,y
    @draginfo.object.x = x
    @draginfo.object.y = y
    update
  end

  #a mouse move event
  def mouseMoveEvent event
    self.cursor = object_at(event.x, event.y).nil? ? @cur1 : @cur2
    if event.buttons == Qt::LeftButton and not @draginfo.nil?
      update_drag_object event.x, event.y unless @draginfo.nil?
    elsif event.buttons == Qt::RightButton
    end  
  end

end
