require 'rubygems'
require 'matrix'
#require 'unprof'
class Physics
include Math

  attr_accessor :widht,:height,:objects,:singularity
  
  def initialize width,height,parent=nil
    @width,@height = width,height
    @center = PhysicsObject.new(@width/2,@height/2)
    @objects = []
    @parent = parent
    @force = Force.new
    @singularity = false
    start_loop
  end
  
  #add an object to the physics module
  def add_object object
    #object need attribute x,y to be valid
    @objects << object    
  end
  
  #apply the forces
  def apply_physics
    while @singularity
      sleep 0.1
    end
    #apply force pushing nodes to center
    @objects.each do |o|
      delta = @force.center_mom(Vector.[](o.x-@center.x,o.y-@center.y))
        o.x += delta[0]; o.y += delta[1]
    end
    #apply force pushing nodes apart
    @objects.each do |o|
      @objects.each do |oo|
        unless o === oo
          delta = @force.field_mom(Vector.[](o.x-oo.x,o.y-oo.y))
          o.x += delta[0]; o.y += delta[1]
        end
      end  
    end
  end
  
  #a loop
  def start_loop
    Thread.new do
      loop {sleep(1/45);update_physics}
    end
  end
  
  #update the physics model
  def update_physics
     unless @parent.nil?
       unless @parent.physics.nil?
        apply_physics
        @parent.update unless @parent.updating
       end 
    end
  end 
end

class Force
  #push nodes apart
  def field_mom(delta)
    r = delta.r
    r = 1e-5 if r < 1e-5
    delta *= 1/r
    delta *= (1./(r**1.5 + 1e-5)*5000)
    return delta 
  end
    
  # delta is PVector, diff between the nodes, deg is the strength of the edge
  def edge_mom(delta, deg)
    r = delta.r
    r = 1e-5 if r < 1e-5
    delta *= 1/r
    delta *= (r*4e-3)
    return delta;
  end
    
  #pull nodes to center
  def center_mom(delta)
    r = delta.r
    r = 1e-5 if r < 1e-5
    delta *= 1/r
    delta *= (r*-1e-2)
    return delta;
  end
end

#simple physics object
class PhysicsObject
  attr_accessor :x,:y
  def initialize x,y
    @x,@y = x,y
  end
end
