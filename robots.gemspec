Gem::Specification.new do |s|
  s.name = %q{robots}
  s.version = "0.2.3.3"

  s.specification_version = 2 if s.respond_to? :specification_version=

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["pangdudu"]
  s.date = %q{2009-07-31}
  s.default_executable = %q{robots}
  s.description = %q{robots!}
  s.email = %q{pangdudu@github}
  s.executables = ["robots"]
  s.extra_rdoc_files = ["README.rdoc"]
  s.files = ["README.rdoc",
   "bin/robots",
   "lib/robots.rb", 
   "lib/robots_agent.rb", 
   "lib/robots_infrastructure.rb",
   "lib/robots_xml.rb",
   "lib/robots_prototype.rb",
   "lib/robots_daemon.rb",
   "config/org.robots.service.conf",
   "config/remote.session.dbus.conf",
   "config/start_dbus_session.sh",
   "funky-examples/visualizer-agent/flow_physics.rb",
   "funky-examples/visualizer-agent/qt_visual.rb",
   "funky-examples/visualizer-agent/visualizer_agent.rb",
   "funky-examples/talking-swift-agent/README",
   "funky-examples/talking-swift-agent/talking_swift_agent.rb",
   "funky-examples/talking-swift-agent/text_generator.rb",
   "funky-examples/memory-agent/memory_agent.rb",
   "funky-examples/memory-agent/memory_generator_agent.rb"]
  s.has_rdoc = true
  s.homepage = %q{http://github.com/pangdudu/robots}
  s.rubyforge_project = %q{http://github.com/pangdudu/robots}
  s.require_paths = ["lib"]
  s.rubygems_version = %q{1.3.1}
  s.summary = %q{more robots!}
  s.add_dependency(%q<pangdudu-ruby-dbus-daemon>, [">= 0"])
  s.add_dependency(%q<pangdudu-ruby-dbus>, [">= 0"])
  s.add_dependency(%q<pangdudu-rofl>, [">= 0"])
  s.add_dependency(%q<hpricot>, [">= 0"])
  s.add_dependency(%q<builder>, [">= 0"])
end
